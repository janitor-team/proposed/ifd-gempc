/**************************************************************
/        Title: usbserial.h
/       Author: David Corcoran
/      Purpose: Abstracts usb API to serial like calls
/      $Id: usbserial.h,v 1.8 2012/04/08 19:01:01 rousseau Exp $
***************************************************************/

#ifndef _USBSERIAL_H_
#define _USBSERIAL_H_

status_t OpenUSB( DWORD lun, DWORD channel );
status_t WriteUSB( DWORD lun, DWORD length, unsigned char *Buffer );
status_t ReadUSB( DWORD lun, DWORD *length, unsigned char *Buffer );
status_t CloseUSB( DWORD lun );

#endif

