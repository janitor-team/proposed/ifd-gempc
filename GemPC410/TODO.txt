TODO list for GemPC410
======================

- support memory cards

DONE
====

- PowerUp 3V then if fails 5V
- support more than one reader
- create /dev/pcsc/[1-4] at module installation
  -> See README.410
- support hot unplug/replug (power off/on on the reader)
  -> should work if pcscd is restarted
- do not reset the card if the command timeout
  -> the card is only reseted if the next command fails. The solution
  with long commands (and without sending time request bytes to the
  reader) is to wait a few seconds just after sending the long command
  (like a key generation on some Gemplus GPL cards)

REJECTED
========
- support GCR400 or GCR410
  it would be a hack on the common part. These readers are not sold
  anymore so support is not needed.

$Id: TODO.txt,v 1.7 2012/04/08 19:09:42 rousseau Exp $

