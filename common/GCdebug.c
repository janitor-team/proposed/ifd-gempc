/*
 * $Id: GCdebug.c,v 1.10 2012/04/08 19:09:47 rousseau Exp $
 * GCdebug.c: log (or not) messages
 * Copyright (C) 2001 Ludovic Rousseau <ludovic.rousseau@free.fr>
 *
 * License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */


#include "Config.h"
#include "GCdebug.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define DEBUG_BUF_SIZE (256*3+30)

static char DebugBuffer[DEBUG_BUF_SIZE];

void log_msg(const int priority, const char *fmt, ...)
{
	va_list argptr;

	(void)priority;
	va_start(argptr, fmt);
	vsnprintf(DebugBuffer, DEBUG_BUF_SIZE, fmt, argptr);
	va_end(argptr);

	fprintf(stderr, "%s\n", DebugBuffer);
} /* debug_msg */

void log_xxd(const int priority, const char *msg, const unsigned char *buffer,
	const int len)
{
	int i;
	char *c, *debug_buf_end;

	(void)priority;
	debug_buf_end = DebugBuffer + DEBUG_BUF_SIZE - 5;

	strncpy(DebugBuffer, msg, sizeof(DebugBuffer)-1);
	c = DebugBuffer + strlen(DebugBuffer);

	for (i = 0; (i < len) && (c < debug_buf_end); ++i)
	{
		sprintf(c, "%02X ", buffer[i]);
		c += strlen(c);
	}

	fprintf(stderr, "%s\n", DebugBuffer);
} /* debug_xxd */

