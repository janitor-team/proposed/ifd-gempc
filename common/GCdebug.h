/*
 * $Id: GCdebug.h,v 1.13 2012/04/08 19:09:47 rousseau Exp $
 * gcdebug.h: log (or not) messages using syslog
 * Copyright (C) 2001 Ludovic Rousseau <ludovic.rousseau@free.fr>
 *
 * License: this code is under a double licence COPYING.BSD and COPYING.GPL
 *
 */

/*
 * DEBUG_CRITICAL("text");
 *	print "text" is DEBUG_LEVEL_CRITICAL and DEBUG_STDERR is defined
 *	send "text" to syslog if DEBUG_LEVEL_CRITICAL is defined
 *
 * DEBUG_CRITICAL2("text: %d", 1234)
 *  print "text: 1234" is DEBUG_LEVEL_CRITICAL and DEBUG_STDERR is defined
 *  send "text: 1234" to syslog if DEBUG_LEVEL_CRITICAL is defined
 * the format string can be anything printf() can understand
 *
 * same thing for DEBUG_INFO and DEBUG_COMM
 *
 * DEBUG_XXD(msg, buffer, size) is only defined if DEBUG_LEVEL_COMM if defined
 *
 */

#ifndef __CONFIG_H__
#error "file Config.h NOT included"
#endif

#ifdef __APPLE__
#include <debuglog.h>
#else
#include <PCSC/debuglog.h>
#endif

#ifndef _GCDEBUG_H_
#define  _GCDEBUG_H_

/* You can't do #ifndef __FUNCTION__ */
#if !defined(__GNUC__) && !defined(__IBMC__)
#define __FUNCTION__ ""
#endif

#define DEBUG_CRITICAL(fmt) Log1(PCSC_LOG_CRITICAL, fmt)
#define DEBUG_CRITICAL2(fmt, data) Log2(PCSC_LOG_CRITICAL, fmt, data)
#define DEBUG_CRITICAL3(fmt, data1, data2) Log3(PCSC_LOG_CRITICAL, fmt, data1, data2)
#define DEBUG_CRITICAL4(fmt, data1, data2, data3) Log4(PCSC_LOG_CRITICAL, fmt, data1, data2, data3)

#define DEBUG_INFO(fmt) Log1(PCSC_LOG_INFO, fmt)
#define DEBUG_INFO2(fmt, data) Log2(PCSC_LOG_INFO, fmt, data)
#define DEBUG_INFO3(fmt, data1, data2) Log3(PCSC_LOG_INFO, fmt, data1, data2)

#define DEBUG_PERIODIC(fmt) Log1(PCSC_LOG_DEBUG, fmt)
#define DEBUG_PERIODIC2(fmt, data) Log2(PCSC_LOG_DEBUG, fmt, data)
#define DEBUG_PERIODIC3(fmt, data1, data2) Log3(PCSC_LOG_DEBUG, fmt, data1, data2)

#define DEBUG_COMM(fmt) Log1(PCSC_LOG_DEBUG, fmt)
#define DEBUG_COMM2(fmt, data) Log2(PCSC_LOG_DEBUG, fmt, data)
#define DEBUG_COMM3(fmt, data1, data2) Log3(PCSC_LOG_DEBUG, fmt, data1, data2)
#define DEBUG_XXD(msg, buffer, size) log_xxd(PCSC_LOG_DEBUG, msg, buffer, size)

#endif

